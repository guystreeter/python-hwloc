#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of the hwloc-distrib utility
# from the hwloc package (no input options). It is NOT complete.
#

from __future__ import print_function
import sys, getopt
import hwloc

version = '1.1'

progname = sys.argv[0]
def usage():
    print('Usage:', progname, '''[options] number
Options:
  --single         Singlify each output to a single CPU
  --taskset        Show taskset-specific cpuset strings
  --ignore=<type>  Ignore objects of the given type
  --from=<type>    Distribute starting from objects of the given type
  --to=<type>      Distribute down to objects of the given type
  --at=<type>      Distribute among objects of the given type
  -v               Show verbose messages
  --version        Report version and exit''', file=sys.stderr)

try:
    options, args = getopt.getopt(sys.argv[1:], 'v', ['single', 'taskset', 'ignore=', 'from=', 'to=', 'at=', 'version'])
except getopt.GetoptError as err:
    print(str(err))
    usage()
    sys.exit(2)

if len(args) != 1:
    usage()
    sys.exit(2)
try:
    number = int(args[0], 10)
except ValueError as err:
    print(str(err))
    usage()
    sys.exit(2)

singlify = False
taskset = False
verbose = False
ignore_type = None
from_type = None
to_type = None
at_type = None

for o, a in options:
    if o == '--version':
        print(progname, version)
        sys.exit(0)
    elif o == '--single':
        singlify = True
    elif o == '--taskset':
        taskset = True
    elif o == '-v':
        verbose = True
    elif o == '--ignore':
        ignore_type = a
    elif o == '--from':
        from_type = a
    elif o == '--to':
        to_type = a
    elif o == '--at':
        from_type = a
        to_type = a

topo = hwloc.Topology()

if ignore_type:
    topo.ignore_type(ignore_type)

if verbose:
    print('distributing', number, file=sys.stderr)

topo.load()

from_depth = 0
try:
    if from_type:
        from_depth = topo.get_type_depth(from_type)
except hwloc.ArgError as err:
    pass
    
if from_depth == hwloc.TYPE_DEPTH_UNKNOWN:
    print('unavailable type', from_type, 'to distribute among, ignoring', file=sys.stderr)
    from_depth = 0
elif from_depth == hwloc.TYPE_DEPTH_MULTIPLE:
    print('multiple depth for type', from_type, 'to distribute among, ignoring', file=sys.stderr)
    from_depth = 0

to_depth = hwloc.INT_MAX
try:
    if to_type:
        to_depth = topo.get_type_depth(to_type)
except hwloc.ArgError as err:
    pass

if to_depth == hwloc.TYPE_DEPTH_UNKNOWN:
    print('unavailable type', to_type, 'to distribute among, ignoring', file=sys.stderr)
    to_depth = hwloc.INT_MAX
elif to_depth == hwloc.TYPE_DEPTH_MULTIPLE:
    print('multiple depth for type', to_type, 'to distribute among, ignoring', file=sys.stderr)
    to_depth = hwloc.INT_MAX

cpusets = topo.distributev(topo.objs_by_depth(from_depth), number, to_depth)

for c in cpusets:
    if singlify:
        c.singlify()
    if taskset:
        print(c.taskset_asprintf())
    else:
        print(str(c))
