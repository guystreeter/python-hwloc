#!/usr/bin/python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of the hwloc-calc utility
# from the hwloc package (no input format support). It is NOT complete.
#

from __future__ import print_function
import sys, getopt, os
import hwloc

from python_hwloc_locations import locations_usage, process_location_args, LocationSyntaxError
import python_hwloc_misc as misc

def calc_output(bitmap):
	if singlify:
		bitmap.singlify()

	if showobjs:
		remaining = bitmap.dup()
		space = ''
		while not bitmap.iszero:
			obj = topology.get_first_largest_obj_inside_cpuset(bitmap)
			string = obj.type_asprintf(1)
			idx = obj.logical_index
			if not logicalo:
				idx = obj.os_index
			if idx == hwloc.INT_MAX:
				print('%s%s' % (space, string), end=' ')
			else:
				print('%s%s: %u' % (space, string, idx), end=' ')
			remaining = remaining.andnot(obj.cpuset)
			space = ' '
		print()
	elif intersectDepth != -1:
		first = True
		proc = topology.get_next_obj_covering_cpuset_by_depth(bitmap, intersectDepth, None)
		for proc in topology.objs_covering_cpuset_by_depth(bitmap, intersectDepth):
			if not first:
				print(',', end=' ')
			if logicalo:
				print(proc.logical_index, end=' ')
			else:
				print(proc.os_index, end=' ')
			first = False
		print()
	else:
		if taskset:
			print(bitmap.taskset_asprintf())
		else:
			print(bitmap)

class TypeDepthError(Exception):
	def __init__(self, type_depth, string):
		self.depth = int(type_depth)
		self.msg = string
	def __str__(self):
		return self.msg
	def __int__(self):
		return self.depth

def type_depth(string):
	type_depth = None
	try:
		objType = hwloc.Obj.type_sscanf(string)[0]
	except OSError:
		objType = None
		try:
			type_depth = int(string)
		except:
			raise TypeDepthError(hwloc.TYPE_DEPTH_UNKNOWN, 'cannot parse '+string)
	return objType, type_depth

def check_type_depth(myType):
	type_depth = -1
	if myType != -1:
		type_depth = topology.get_type_depth(myType)
		if type_depth == hwloc.TYPE_DEPTH_UNKNOWN:
			raise TypeDepthError(type_depth, 'unavlailable type '+hwloc.Obj.string_of_type(myType))
		if type_depth == hwloc.TYPE_DEPTH_MULTIPLE:
			raise TypeDepthError(type_depth, 'cannot use type %s, it has multiple depth' % (hwloc.Obj.string_of_type(myType),))
		return type_depth
	return -1

def usage(where):
	print('Usage:', progname, '''[options] <location> ...
 <location> may be a space-separated list of cpusets or objects
            as supported by the hwloc-bind utility, e.g.:''',
    locations_usage,
    '''\nConversion options:
  [default]                 Report the combined input locations as a CPU set
  --number-of <type|depth>
  -N <type|depth>           Report the number of objects intersecting the CPU set
  --intersect <type|depth>
  -I <type|depth>           Report the indexes of object intersecting the CPU set
  --hierarchical <type1>.<type2>...
  -H <type1>.<type2>...     Find the list of objects intersecting the CPU set and
                            display them as hierarchical combinations such as
                            type1:index1.type2:index2...
  --largest                 Report the list of largest objects in the CPU set
Formatting options:
  -l --logical              Use logical object indexes (default)
  -p --physical             Use physical object indexes
  --li --logical-input      Use logical indexes for input (default)
  --lo --logical-output     Use logical indexes for output (default)
  --pi --physical-input     Use physical indexes for input
  --po --physical-output    Use physical indexes for output
  --sep <sep>               Use separator <sep> in the output
  --taskset                 Manipulate taskset-specific cpuset strings
  --single                  Singlify the output to a single CPU
Input topology options:
  --restrict <cpuset>       Restrict the topology to processors listed in <cpuset>
  --whole-system            Do not consider administration limitations''',
    misc.input_format_usage(10),
    '''\nMiscellaneous options:
  -q --quiet                Hide non-fatal error messages
  -v --verbose              Show verbose messages
  --version                 Report version and exit''', file=where)

short_opts = 'lpqvN:I:H:' + misc.input_format_short
long_opts = ['number-of=',
			 'intersect=',
			 'hierarchical=',
			 'largest',
			 'pulist',
			 'proclist',
			 'nodelist',
			 'logical',
			 'physical',
			 'li', 'logical-input',
			 'lo', 'logical-output',
			 'pi', 'physical-input',
			 'po', 'physical-output',
			 'sep=',
			 'taskset',
			 'single',
			 'restrict=',
			 'whole-system',
			 'quiet',
			 'verbose',
			 'version',] + misc.input_format_long

progname = sys.argv[0]

try:
	options, args = getopt.gnu_getopt(sys.argv[1:], short_opts, long_opts)
except getopt.GetoptError as err:
	print(str(err))
	usage(sys.stderr)
	sys.exit(2)


verbose = 0
logicali = True
logicalo = True
numberOfType = None
numberOfDepth = None
intersectDepth = -1
intersectType = -1
heirdepth = []
heirtype = []
showobjs = False
singlify = False
taskset = False
synthetic = None

flags = hwloc.TOPOLOGY_FLAG_WHOLE_IO | hwloc.TOPOLOGY_FLAG_ICACHES
input_source = None
input_format = misc.INPUT_DEFAULT
input_changed = False

os.environ['HWLOC_XML_VERBOSE'] = '1'
os.environ['HWLOC_SYNTHETIC_VERBOSE'] = '1'

topology = hwloc.Topology()
topology.set_flags(flags)
topology.load()
depth = topology.depth

for o, a in options:
	if o in ('-v', '--verbose'):
		verbose += 1
	elif o in ('-q', '--quiet'):
		verbose -= 1
	elif o == '--whole-system':
		flags |= hwloc.TOPOLOGY_FLAG_WHOLE_SYSTEM
		input_changed = True
	elif o == '--help':
		usage(sys.stdout)
		sys.exit(0)
	elif o == '--restrict':
		restrictset = hwloc.Bitmap.alloc(a)
		try:
			topology.restrict(restrictset, 0)
		except OSError as e:
			print(e.strerror, e.errno, 'Restricting the topology')
			# fallthrough
	elif o in ('--number-of', '-N'):
		if a is None:
			usage(sys.stdout)
			sys.exit(0)
		try:
			numberOfType, numberOfDepth = type_depth(a)
		except:
			print('unrecognized --number-of type or depth '+a, file=sys.stderr)
			usage(sys.stderr)
			sys.exit(0)
	elif o in ('-I', '--intersect'):
		if a is None:
			usage(sys.stdout)
			sys.exit(0)
		try:
			intersectType, intersectDepth = type_depth(a)
		except:
			print('unrecognized --intersect type or depth '+a, file=sys.stderr)
			usage(sys.stderr)
			sys.exit(0)
	elif o in ('-H', '--hierarchical'):
		if a is None:
			usage(sys.stdout)
			sys.exit(0)
		for h in a.split('.'):
			try:
				t, d = type_depth(h)
				heirtype.append(t)
				heirdepth.append(d)
			except:
				print('unrecognized --intersect type or depth '+a, file=sys.stderr)
				usage(sys.stderr)
				sys.exit(0)
	elif o in ('--pulist', '--proclist'):
		intersectType = hwloc.OBJ_PU
	elif o == '--nodelist':
		intersectType = hwloc.OBJ_NODE
	elif o in ('--largest', '--objects'):
		showobjs = True
	elif o == '--version':
		print(progname, hwloc.version_string())
		sys.exit(0)
	elif o in ('-l', '--logical'):
		logicali = True
		logicalo = True
	elif o in ('--li', '--logical-input'):
		logicali = True
	elif o in ('--lo', '--logical-output'):
		logicalo = True
	elif o in ('-p', '--physical'):
		logicali = False
		logicalo = False
	elif o in ('--pi', '--physical-input'):
		logicali = False
	elif o in ('--po', '--physical-output'):
		logicalo = False
	elif o == '--sep':
		if a is None:
			usage(sys.stdout)
			sys.exit(0)
		outsetp = a
	elif o == '--single':
		singlify = True
	elif o == '--synthetic':
		synthetic = a
		input_changed = True
	elif o == '--taskset':
		taskset = True
	elif o in misc.input_funcs:
		i, input_source = misc.input_funcs[o](o, a)
		if i is not None:
			input_format = i
		input_changed = True
	else:
		print('Unrecognized option:', o, file=sys.stderr)
		sys.exit(-1)

	if input_changed:
		topology = hwloc.Topology()
		topology.set_flags(hwloc.TOPOLOGY_FLAG_WHOLE_IO)
		if synthetic:
			topology.set_synthetic(synthetic)
		if input_source:
			misc.enable_input_format(topology, input_source, input_format, verbose)
		topology.load()

	if not len(args):
		args = []
		line = sys.stdin.readline()
		while line:
			args += line.strip().split()
			line = sys.stdin.readline()
try:
	result = process_location_args(topology, args, logicali, taskset, verbose)
except LocationSyntaxError as s:
	print('ignored unrecognized argument', str(s), file=sys.stderr)
	sys.exit(-1)

if intersectType is not None:
	try:
		intersectDepth = check_type_depth(intersectType)
	except TypeDepthError as e:
		print('intersect', str(e))
calc_output(result)
