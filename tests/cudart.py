#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2013-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/cudart.c from the
# hwloc package. This test is limited for lack of python bindings for the
# cuda runtime library.
#

from __future__ import print_function
import hwloc

topology = hwloc.Topology()
topology.set_flags(hwloc.TOPOLOGY_FLAG_IO_DEVICES)
topology.load()

i = 0
while True:
    osdev = topology.cudart_get_device_osdev_by_index(i)
    if osdev is None:
        break

    ancestor = osdev.non_io_ancestor
    print('found OSDev', osdev.name)
    assert osdev.name.startswith('cuda')
    assert int(osdev.name[4:]) == i

    assert osdev.get_info_by_name('Backend') == 'CUDA'

    assert osdev.attr.osdev.type == hwloc.OBJ_OSDEV_COPROC

    assert osdev.get_info_by_name('CoProcType') == 'CUDA'

    print('found OSDev model', osdev.get_info_by_name('GPUModel'))

    i += 1
