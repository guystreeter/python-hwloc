#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_get_largest_objs_inside_cpuset.c
# from the hwloc package.
#

import hwloc

SYNTHETIC_TOPOLOGY_DESCRIPTION = '6 5 4 3 2'  # 736bits wide topology
# first and last(735th) bit set
GIVEN_LARGESPLIT_CPUSET_STRING = '8000,,,,,,,,,,,,,,,,,,,,,,1'
# 736th bit is too large for the 720-wide topology
GIVEN_TOOLARGE_CPUSET_STRING = '10000,,,,,,,,,,,,,,,,,,,,,,0'
GIVEN_HARD_CPUSET_STRING = '07ff,ffffffff,e0000000'

OBJ_MAX = 16

topo = hwloc.Topology()

topo.set_synthetic(SYNTHETIC_TOPOLOGY_DESCRIPTION)

topo.load()

depth = topo.depth

# just get the system object
obj = topo.root_obj
objs = topo.get_largest_objs_inside_cpuset(obj.cpuset, 1)
assert len(objs) == 1
assert objs[0] == obj
assert obj == topo.get_first_largest_obj_inside_cpuset(obj.cpuset)

# get just the very last object
obj = topo.get_obj_by_depth(depth - 1, topo.get_nbobjs_by_depth(depth - 1) - 1)
objs = topo.get_largest_objs_inside_cpuset(obj.cpuset, 1)
assert len(objs) == 1
assert objs[0] == obj

# try an empty one
set1 = hwloc.Bitmap.alloc()
objs = topo.get_largest_objs_inside_cpuset(set1, 1)
assert len(objs) == 0
assert topo.get_first_largest_obj_inside_cpuset(set1) == None

# try an impossible one
set1 = hwloc.Bitmap.alloc(GIVEN_TOOLARGE_CPUSET_STRING)
try:
    objs = topo.get_largest_objs_inside_cpuset(set1, 1)
except hwloc.ArgError:
    pass
else:
    raise AssertionError('too large cpuset returned results')

# try a harder one with 1 obj instead of 2 needed
set1 = hwloc.Bitmap.alloc(GIVEN_LARGESPLIT_CPUSET_STRING)
objs = topo.get_largest_objs_inside_cpuset(set1, 1)
assert len(objs) == 1
assert objs[0] == topo.get_obj_by_depth(depth - 1, 0)
assert topo.get_first_largest_obj_inside_cpuset(
    set1) == topo.get_obj_by_depth(depth - 1, 0)
# try a harder one with lots of objs instead of 2 needed
objs = topo.get_largest_objs_inside_cpuset(set1, 2)
assert len(objs) == 2
assert objs[0] == topo.get_obj_by_depth(depth - 1, 0)
assert objs[1] == topo.get_obj_by_depth(
    depth - 1, topo.get_nbobjs_by_depth(depth - 1) - 1)
obj0 = topo.get_first_largest_obj_inside_cpuset(set1)
set1 = set1.andnot(obj0.cpuset)
obj1 = topo.get_first_largest_obj_inside_cpuset(set1)
set1 = set1.andnot(obj1.cpuset)
obj2 = topo.get_first_largest_obj_inside_cpuset(set1)
assert obj0 == topo.get_obj_by_depth(depth - 1, 0)
assert obj1 == topo.get_obj_by_depth(
    depth - 1, topo.get_nbobjs_by_depth(depth - 1) - 1)
assert obj2 is None
assert set1.iszero

# try a very hard one
set1 = hwloc.Bitmap.alloc(GIVEN_HARD_CPUSET_STRING)
objs = topo.get_largest_objs_inside_cpuset(set1, OBJ_MAX)
assert objs[0] == topo.get_obj_by_depth(5, 29)
assert objs[1] == topo.get_obj_by_depth(3, 5)
assert objs[2] == topo.get_obj_by_depth(3, 6)
assert objs[3] == topo.get_obj_by_depth(3, 7)
assert objs[4] == topo.get_obj_by_depth(2, 2)
assert objs[5] == topo.get_obj_by_depth(4, 36)
assert objs[6] == topo.get_obj_by_depth(5, 74)
