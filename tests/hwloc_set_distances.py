#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of
# tests/hwloc_set_distances.c
# from the hwloc package.
#

import hwloc
import os

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')

# default 2*8*1
topology.load()
depth = topology.depth
assert depth == 4
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 16
width = topology.get_nbobjs_by_depth(3)
assert width == 16

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')

# 2*8*1 and group 8 cores as 2*2*2
indexes = list(range(16))


def values():
    for i in range(16):
        for j in range(16):
            if i == j:
                yield 3
            elif i // 2 == j // 2:
                yield 5
            elif i // 4 == j // 4:
                yield 7
            else:
                yield 9
distances = list(values())
topology.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
topology.load()
depth = topology.depth
assert depth == 6
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 4
width = topology.get_nbobjs_by_depth(3)
assert width == 8
width = topology.get_nbobjs_by_depth(4)
assert width == 16

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:8 pu:1')

# revert to default 2*8*1
topology.set_distance_matrix(hwloc.OBJ_CORE, [], [])
topology.load()
depth = topology.depth
assert depth == 4
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 16
width = topology.get_nbobjs_by_depth(3)
assert width == 16

topology = hwloc.Topology()

# default 2*4*4
topology.set_synthetic('node:2 core:4 pu:4')
topology.set_distance_matrix(hwloc.OBJ_CORE, [], [])
topology.load()
depth = topology.depth
assert depth == 4
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 8
width = topology.get_nbobjs_by_depth(3)
assert width == 32

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:4 pu:4')
topology.set_distance_matrix(hwloc.OBJ_CORE, [], [])

# 2*4*4 and group 4 cores as 2*2
os.environ['HWLOC_Core_DISTANCES'] = '0,1,2,3,4,5,6,7:4*2'
topology.load()
depth = topology.depth
assert depth == 5
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 4
width = topology.get_nbobjs_by_depth(3)
assert width == 8
width = topology.get_nbobjs_by_depth(4)
assert width == 32

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:4 pu:4')
topology.set_distance_matrix(hwloc.OBJ_CORE, [], [])

# 2*4*4 and group 4cores as 2*2 and 4PUs as 2*2
os.environ['HWLOC_PU_DISTANCES'] = '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31:16*2'
topology.load()
depth = topology.depth
assert depth == 6
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 4
width = topology.get_nbobjs_by_depth(3)
assert width == 8
width = topology.get_nbobjs_by_depth(4)
assert width == 16
width = topology.get_nbobjs_by_depth(5)
assert width == 32

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:4 pu:4')
topology.set_distance_matrix(hwloc.OBJ_CORE, [], [])

# replace previous core distances with useless ones (grouping as the existing numa nodes)
# 2*4*4 and group 4PUs as 2*2
os.environ['HWLOC_Core_DISTANCES'] = '0,1,2,3,4,5,6,7:2*4'
topology.load()
depth = topology.depth
assert depth == 5
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 8
width = topology.get_nbobjs_by_depth(3)
assert width == 16
width = topology.get_nbobjs_by_depth(4)
assert width == 32

topology = hwloc.Topology()
topology.set_synthetic('node:2 core:4 pu:4')
topology.set_distance_matrix(hwloc.OBJ_CORE, [], [])

# clear everything
# default 2*4*4
os.environ['HWLOC_Core_DISTANCES'] = 'none'
os.environ['HWLOC_PU_DISTANCES'] = 'none'
topology.load()
depth = topology.depth
assert depth == 4
width = topology.get_nbobjs_by_depth(0)
assert width == 1
width = topology.get_nbobjs_by_depth(1)
assert width == 2
width = topology.get_nbobjs_by_depth(2)
assert width == 8
width = topology.get_nbobjs_by_depth(3)
assert width == 32

# buggy tests
try:
    topology.set_distance_matrix(hwloc.OBJ_CORE, indexes, None)
    assert False
except:
    pass
try:
    topology.set_distance_matrix(hwloc.OBJ_CORE, None, distances)
    assert False
except:
    pass
try:
    topology.set_distance_matrix(hwloc.OBJ_CORE, (0, 1), (0, 1))
    assert False
except:
    pass
try:
    topology.set_distance_matrix(
        hwloc.OBJ_CORE, (0, 1), (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    assert False
except:
    pass
indexes[1] = 0
try:
    topology.set_distance_matrix(hwloc.OBJ_CORE, indexes, distances)
    assert False
except:
    pass
