#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This tests the python hwloc_obj implementation
#

from __future__ import print_function
import hwloc

topo = hwloc.Topology()
topo.set_synthetic('machine:3 group:2 group:2 core:3 cache:2 cache:2 2')
topo.load()

# root_obj is a topology property
obj = topo.root_obj

# properties of the Obj

assert not obj.parent
assert obj.attr
# objs are equal if they are physically the same structure
assert obj.first_child.parent == obj
assert not obj.next_cousin
assert not obj.userdata

# children is an iterator
for c in obj.children:
    assert c.parent == obj

last = topo.get_obj_by_depth(
    topo.depth - 1, topo.get_nbobjs_by_depth(topo.depth - 1) - 1)
other = last.prev_sibling
assert other

assert last.type_string == other.type_string
# type_of_string is a class method
assert last.type == hwloc.Obj.type_of_string(other.type_asprintf(0))

other = last.parent
print(other.type_string, other.attr_asprintf(';', 1))

topo = hwloc.Topology()
topo.load()
obj = topo.root_obj
for i in obj.infos:
    print(i.name, i.value)
