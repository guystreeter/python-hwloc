#!/usr/bin/env python3
# -*- python -*-

#
# Copyright (C) 2011-2017 Red Hat, Inc.
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.2.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#
# This is a re-implementation in python of tests/hwloc_bitmap.c
# from the hwloc package.
#

import hwloc

Set = None

# check an empty bitmap
Set = hwloc.Bitmap.alloc()
assert Set.ulong() == 0
assert Set.ulong(0) == 0
assert Set.ulong(1) == 0
assert Set.ulong(23) == 0
# check a non-empty bitmap
Set.from_ulong(0xff, 4)
assert Set.ulong(4) == 0xff
assert Set.ulong() == 0
assert Set.ulong(0) == 0
assert Set.ulong(1) == 0
assert Set.ulong(23) == 0
# check a two-long bitmap
Set.from_ulong(0xfe, 4)
Set.set_ulong(0xef, 6)
assert Set.ulong(4) == 0xfe
assert Set.ulong(6) == 0xef
assert Set.ulong() == 0
assert Set.ulong(0) == 0
assert Set.ulong(1) == 0
assert Set.ulong(23) == 0
# check a zeroed bitmap
Set.zero()
assert Set.ulong() == 0
assert Set.ulong(0) == 0
assert Set.ulong(1) == 0
assert Set.ulong(4) == 0
assert Set.ulong(23) == 0
Set = None

# check a full bitmap
Max = 0xFFFFFFFFFFFFFFFF
Set = hwloc.Bitmap.alloc_full()
assert Set.ulong() == Max
assert Set.ulong(0) == Max
assert Set.ulong(1) == Max
assert Set.ulong(23) == Max
# check a almost full bitmap
Set.set_ulong(0xff, 4)
assert Set.ulong(4) == 0xff
assert Set.ulong() == Max
assert Set.ulong(0) == Max
assert Set.ulong(1) == Max
assert Set.ulong(23) == Max
# check a almost empty bitmap
Set.from_ulong(0xff, 4)
assert Set.ulong(4) == 0xff
assert Set.ulong() == 0
assert Set.ulong(0) == 0
assert Set.ulong(1) == 0
assert Set.ulong(23) == 0
Set = None

# check ranges
Set = hwloc.Bitmap.alloc()
assert Set.weight() == 0
# 23-45
Set.set_range(23, 45)
assert Set.weight() == 23
# 23-45,78-
Set.set_range(78, -1)
assert Set.weight() == -1
# 23-
Set.set_range(44, 79)
assert Set.weight() == -1
assert Set.first == 23
assert not Set.isfull
# 0-
Set.set_range(0, 22)
assert Set.weight() == -1
assert Set.isfull
# 0-34,57-
Set.clr_range(35, 56)
assert Set.weight() == -1
assert not Set.isfull
# 0-34,57
Set.clr_range(58, -1)
assert Set.weight() == 36
assert Set.last == 57
assert not Set.isfull
# 0-34
Set.clr(57)
assert Set.weight() == 35
assert Set.last == 34
# empty
Set.clr_range(0, 34)
assert Set.weight() == 0
assert Set.first == -1
Set = None

# check miscellaneous other functions
Set = hwloc.Bitmap.alloc()
# from_ulong
Set.from_ulong(0x0ff0)
assert Set.first == 4
assert Set.last == 11
assert Set.weight() == 8
assert Set.ulong(0) == 0x0ff0
assert Set.ulong(1) == 0
# from_ith_ulong
Set.zero()
assert Set.weight() == 0
Set.from_ulong(0xff00, 2)
assert Set.weight() == 8
assert Set.ulong(0) == 0
assert Set.ulong(1) == 0
assert Set.ulong(2) == 0xff00
assert Set.ulong(0) == 0
# allbut and not
Set.allbut(153)
assert Set.weight() == -1
Set = ~Set
assert Set.weight() == 1
assert Set.first == 153
assert Set.last == 153
# clr_range
Set.fill()
Set.clr_range(178, 3589)
Set = ~Set
assert Set.weight() == 3589 - 178 + 1
assert Set.first == 178
assert Set.last == 3589
# singlify
Set.zero()
Set.set_range(0, 127)
assert Set.weight() == 128
Set = ~Set
assert Set.weight() == -1
Set.singlify()
assert Set.weight() == 1
assert Set.first == 128
assert Set.last == 128

Set = None
